﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.block = new System.ComponentModel.BackgroundWorker();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.brows = new System.ComponentModel.BackgroundWorker();
            this.screenshot = new System.ComponentModel.BackgroundWorker();
            this.processstart = new System.ComponentModel.BackgroundWorker();
            this.camera = new System.ComponentModel.BackgroundWorker();
            this.file_op = new System.ComponentModel.BackgroundWorker();
            this.get_process = new System.ComponentModel.BackgroundWorker();
            this.win_op = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.kills = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.fileSystemWatcher2 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher2)).BeginInit();
            this.SuspendLayout();
            // 
            // block
            // 
            this.block.DoWork += new System.ComponentModel.DoWorkEventHandler(this.block_DoWork);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed_1);
            // 
            // brows
            // 
            this.brows.DoWork += new System.ComponentModel.DoWorkEventHandler(this.brows_DoWork);
            // 
            // screenshot
            // 
            this.screenshot.DoWork += new System.ComponentModel.DoWorkEventHandler(this.screenshot_DoWork);
            // 
            // processstart
            // 
            this.processstart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.processstart_DoWork);
            // 
            // camera
            // 
            this.camera.DoWork += new System.ComponentModel.DoWorkEventHandler(this.camera_DoWork);
            // 
            // file_op
            // 
            this.file_op.DoWork += new System.ComponentModel.DoWorkEventHandler(this.file_op_DoWork_1);
            // 
            // get_process
            // 
            this.get_process.DoWork += new System.ComponentModel.DoWorkEventHandler(this.get_process_DoWork);
            // 
            // win_op
            // 
            this.win_op.DoWork += new System.ComponentModel.DoWorkEventHandler(this.win_op_DoWork);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Monitoring Starts.......";
            // 
            // kills
            // 
            this.kills.DoWork += new System.ComponentModel.DoWorkEventHandler(this.kills_DoWork);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // fileSystemWatcher2
            // 
            this.fileSystemWatcher2.EnableRaisingEvents = true;
            this.fileSystemWatcher2.SynchronizingObject = this;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker block;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker brows;
        private System.ComponentModel.BackgroundWorker screenshot;
        private System.ComponentModel.BackgroundWorker processstart;
        private System.ComponentModel.BackgroundWorker camera;
        private System.ComponentModel.BackgroundWorker file_op;
        private System.ComponentModel.BackgroundWorker get_process;
        private System.ComponentModel.BackgroundWorker win_op;
        private System.ComponentModel.BackgroundWorker kills;
        private System.Windows.Forms.Label label2;
        private System.IO.FileSystemWatcher fileSystemWatcher2;
    }
}