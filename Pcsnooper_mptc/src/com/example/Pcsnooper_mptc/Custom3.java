package com.example.pcsnooper_mptc;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class Custom3 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom3);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.custom3, menu);
		return true;
	}

}
