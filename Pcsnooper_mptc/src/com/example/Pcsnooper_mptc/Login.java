package com.example.pcsnooper_mptc;


import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener {

	String namespace="http://tempuri.org/";
	String method="login";
	String soapaction="http://tempuri.org/login";
	
	public static String url="http://169.254.123.60/pcsnooper%20madin1/WebService.asmx";
	EditText ed1,ed2;
	Button b1;
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		b1=(Button)findViewById(R.id.b1);
		b1.setOnClickListener(this);
		ed1=(EditText)findViewById(R.id.editText1);
		ed2=(EditText)findViewById(R.id.editText2);
		SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String ip=sh.getString("ip", "");
		//Toast.makeText(getApplicationContext(), "ip="+ip, Toast.LENGTH_LONG).show();
		url="http://"+ip+"/pcsnooper%20madin1/WebService.asmx";
		 try
		    {
		    if(android.os.Build.VERSION.SDK_INT>9)
		    {
		    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    	android.os.StrictMode.setThreadPolicy(th);
		    }}
		    catch(Exception ex)
		    {
		    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		    }
		
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		
		// TODO Auto-generated method stub
		if(b1==arg0)
		{
			String name=ed1.getText().toString();
			String pwd=ed2.getText().toString();
		 if(name.equalsIgnoreCase(""))
		 {
			 ed1.setError("Name Field is empty");
		 }
		 if(pwd.equalsIgnoreCase(""))
		 {
			 ed2.setError("Password Field is empty");
		 }
		 else
		 {
			try
			{
				SoapObject soap=new SoapObject(namespace, method);
				soap.addProperty("name", name);
				soap.addProperty("pwd", pwd);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
					Intent i=new Intent(getApplicationContext(), View_system.class);
					startActivity(i);
				
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		
			
		}
	}

}}
