package com.example.pcsnooper_mptc;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Customthree extends BaseAdapter{

	
	private Context Context;
	String[] c;
	String[] d;
	String[]f;

	public Customthree(Context applicationContext, String[] date, String[] not,String[]time) {

		this.Context=applicationContext;
		this.c=date;
		this.d=not;
		this.f=time;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return c.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertview, ViewGroup parent) {

		// TODO Auto-generated method stub
LayoutInflater inflator=(LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View gridView;
		if(convertview==null)
		{
			gridView=new View(Context);
			//gridView=inflator.inflate(R.layout.customview, null);
			gridView=inflator.inflate(R.layout.activity_cu3, null);
			
		}
		else
		{
			gridView=(View)convertview;
			
		}
		
		TextView tv1=(TextView)gridView.findViewById(R.id.textView1);
		TextView tv2=(TextView)gridView.findViewById(R.id.textView2);
		TextView tv3=(TextView)gridView.findViewById(R.id.textView3);
		tv1.setTextColor(Color.BLACK);
		tv2.setTextColor(Color.BLACK);
		tv3.setTextColor(Color.BLACK);
		tv1.setText(c[position]);
		tv2.setText(d[position]);
		tv3.setText(f[position]);
		
		return gridView;
	}

}
