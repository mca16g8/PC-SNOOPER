package com.example.pcsnooper_mptc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SystemFiles extends Activity implements OnItemClickListener {
	
	ListView list;
	String[] driv;
	
	final String ip=View_system.ipadr;
	final int port=5556;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_system_files);
		list=(ListView)findViewById(R.id.listView1);
		 //Toast.makeText(getApplicationContext(),ip+"_", Toast.LENGTH_LONG).show();
		mytask m=new mytask();
		m.execute("file");
		Toast.makeText(getApplicationContext(),ip+"", Toast.LENGTH_LONG).show();		
		
		list.setOnItemClickListener(this);
	}

	public class mytask extends AsyncTask<String, Void, String> {
		
	 
	    protected String doInBackground(String... str)
	    {
	    	String drives="";
	   	 try{
	   	 		Socket client = new Socket(ip,port);  //connect to server
				PrintWriter printwriter = new PrintWriter(client.getOutputStream(),true);
				 printwriter.write(str[0]);  //write the message to output stream
				 Log.d("hh", "hhhhhhhhhhhhh");
				 printwriter.flush();
				 
				 BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
				 
			     drives =inFromClient.readLine();
			     driv=drives.split("-");
			     Log.d("dr", drives);
			     
			     for(int i=0; i<driv.length; i++)
			    	 Log.d("dr", driv[i]);					
				       
				 client.close();
	   	 }
	   	 catch(Exception ex)
	   	 {
	   		 ex.printStackTrace();
	   	 }
	   	 

	         return drives;
	    }

	    protected void onProgressUpdate(Integer... progress) {
	       // setProgressPercent(progress[0]);
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	// TODO Auto-generated method stub
	    	super.onPostExecute(result);
	    	ArrayAdapter<String> adpt = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, result.split("-"));
			list.setAdapter(adpt);
	    }
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), ip+"", Toast.LENGTH_LONG).show();
		
		Intent i=new Intent(getApplicationContext(),FilesFetch.class);
		i.putExtra("cmd", driv[arg2]);
		startActivity(i);
	}
}
