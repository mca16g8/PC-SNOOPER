package com.example.pcsnooper_mptc;

import java.net.URL;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Screenshot extends Activity implements OnClickListener {
	Button b1;
	ImageView img;
	String namespace="http://tempuri.org/";
	String method="insert_screenshot";
	String soapaction="http://tempuri.org/insert_screenshot";

	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screenshot);
		b1=(Button)findViewById(R.id.b1);
		img=(ImageView)findViewById(R.id.img1);
	    b1.setOnClickListener(this);
	    try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.screenshot, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0==b1)
		{
			SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		     String mc=sh.getString("mac", "");
		     String ip=sh.getString("ip", "");
		     //Toast.makeText(getApplicationContext(), "mac="+mc, Toast.LENGTH_LONG).show();
		     //Toast.makeText(getApplicationContext(), "ip="+ip, Toast.LENGTH_LONG).show();
			 String ss="http://"+ip+"/pcsnooper%20madin1/scr"+mc+".jpg";
			//Toast.makeText(getApplicationContext(), "ss="+ss, Toast.LENGTH_LONG).show();
			try
			{
				URL u=new URL(ss);
				Bitmap bmp=BitmapFactory.decodeStream(u.openStream());
				img.setImageBitmap(bmp);
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
			
		}

	}

}
