package com.example.pcsnooper_mptc;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Browsing_history extends Activity {
	
	String namespace="http://tempuri.org/";
	String method="view_browsing_his";
	String soapaction="http://tempuri.org/view_browsing_his";
	String[]date,url,uu;
	ListView lv;
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override	
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browsing_history);
		lv=(ListView)findViewById(R.id.listView1);
		
		try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
	 try
		{
		 SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 String mc=sh.getString("mac", "");
		 //Toast.makeText(getApplicationContext(), "mac="+mc, Toast.LENGTH_LONG).show();
			SoapObject soap=new SoapObject(namespace, method);
		soap.addProperty("mac", mc);
			SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
			snv.dotNet=true;
			snv.setOutputSoapObject(soap);
			HttpTransportSE h=new HttpTransportSE(Login.url);
			h.call(soapaction, snv);
			String res=snv.getResponse().toString();
			//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
			if(res.equalsIgnoreCase("no"))
			{
				Toast.makeText(getApplicationContext(), "no value", Toast.LENGTH_LONG).show();
			}
			else
			{
				String[]ar=res.split("\\~");
				date=new String[ar.length];
				url=new String[ar.length];
				uu=new String[ar.length];
				
				
				for(int i=0;i<ar.length;i++)
				{
					String[]ar1=ar[i].split("\\^");
					date[i]=ar1[0];
				    url[i]=ar1[1];
				    uu[i]=ar1[2];
					
				}
				lv.setAdapter(new Customthree(getApplicationContext(), date, uu, url));
			//ArrayAdapter<String>ad=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,uu);
			//lv.setAdapter(ad);
			}
			
		}
		catch(Exception ex)
		{
			Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		}
	
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.browsing_history, menu);
		return true;
	}

}
