package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Process_start extends Activity implements OnClickListener {
	Button b1;
    EditText ed1;
	String namespace="http://tempuri.org/";
	String method="get_process";
	String soapaction="http://tempuri.org/get_process";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_process_start);
	    b1=(Button)findViewById(R.id.b9);
	    ed1=(EditText)findViewById(R.id.editText1);
	    b1.setOnClickListener(this);
	    try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	
	    }
	    

	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.process_start, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0==b1)
		{
			SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String ss=sh.getString("mac", "");
			String sname=ed1.getText().toString();
			if(sname.equalsIgnoreCase(""))
			{
				ed1.setError("Empty Field");
			}
			else
			{
			try
			{
				SoapObject soap=new SoapObject(namespace, method);
				soap.addProperty("mac", ss);
				soap.addProperty("name", sname);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
			
					Intent i=new Intent(getApplicationContext(), View_system.class);
					startActivity(i);
				
				
				
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
			
		}
		
	}

		
	}}
