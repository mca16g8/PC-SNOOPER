package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class System_add extends Activity implements OnClickListener {
	Button b1,b2;
	EditText ed1,ed2;
	String namespace="http://tempuri.org/";
	String method="insert_system";
	String soapaction="http://tempuri.org/insert_system";
//	String url="";

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_system_add);
	    b1=(Button)findViewById(R.id.b1);
	  	    ed1=(EditText)findViewById(R.id.editText1);
	    ed2=(EditText)findViewById(R.id.editText2);
	    b1.setOnClickListener(this);
	    try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	
	    }
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.system_add, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		if(arg0==b1)
		{
			String sname=ed1.getText().toString();
			String mac=ed2.getText().toString();
			if(sname.equalsIgnoreCase(""))
			{
				ed1.setError("Pls enter system name");
			}
			if(mac.equalsIgnoreCase(""))
			{

				ed2.setError("Enter MAC address");
				
			}
			else
			{
			try
			{
				SoapObject soap=new SoapObject(namespace, method);
				soap.addProperty("name", sname);
				soap.addProperty("mac", mac);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
			
					Intent i=new Intent(getApplicationContext(), View_system.class);
					startActivity(i);
				
				
				
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
			
		}
		
	}

}
}