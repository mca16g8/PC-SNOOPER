package com.example.pcsnooper_mptc;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Ipset extends Activity implements OnClickListener {
	EditText ed1;
	Button b1;
	@Override

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ipset);
		b1=(Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		ed1=(EditText)findViewById(R.id.editText1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ipset, menu);
		return true;
		
	}
public String check(String s)
{
	String res="";

	try
	{
	Integer a1=Integer.parseInt(s);
	if(a1>0 && a1<255)
	{
		res="ok";
	}
	else
	{
		res="error";
	}
	return res;
	}
	catch(Exception ex)
	{
		return "error";
	}
}
	@Override
	public void onClick(View arg0) 
	{if(arg0==b1)
	{
		
		int flg=0;
		
		String ip=ed1.getText().toString();
		if(ip.equalsIgnoreCase(""))
		{
		ed1.setError("Enter an IP");
		}
		else
		{
			String [] ip1=ip.split("\\.");
			if(ip1.length!=4)
			{
				ed1.setError("Invalid IP");
			}
			else
			{
				for(int i=0;i<ip1.length;i++)
				{
					String res1=check(ip1[i]);
					if(res1.equalsIgnoreCase("error"))
					{
						flg++;
						break;
					}
						
				}
				if(flg!=0)
				{
					ed1.setError("Invalid IP");
				}
				else
				{
		SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		Editor ed=sh.edit();
		ed.putString("ip", ip);
		ed.commit();
		Intent i=new Intent(getApplicationContext(), Login.class);
		startActivity(i);
				}
			}
		}
	}}

}
