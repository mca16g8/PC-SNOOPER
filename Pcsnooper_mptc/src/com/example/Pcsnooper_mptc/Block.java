package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Block extends Activity implements OnClickListener {
	EditText ed1;
	Button  b1;
	String namespace="http://tempuri.org/";
	String method="insert_block";
	String soapaction="http://tempuri.org/insert_block";
	
	
	
	//String url="http://169.254.247.135/PC%20SNOOPER/WebService.asmx";
			

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_block);
ed1=(EditText )findViewById(R.id.editText1 );
		
		b1=(Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		try
		{
			if(android.os.Build.VERSION.SDK_INT>9)
			{
				ThreadPolicy th=new StrictMode .ThreadPolicy.Builder().permitAll().build();
				android.os.StrictMode .setThreadPolicy(th);
			}
			}
		catch(Exception ex)
		{
			Toast.makeText(getApplicationContext(), "error"+ex.getMessage(),Toast.LENGTH_LONG).show();
			
			
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.block, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
String name=ed1.getText().toString();
		
		try
		{	 SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 String mc=sh.getString("mac", "");
			
			SoapObject soap=new SoapObject(namespace,method);
			soap.addProperty("name", name);
			soap.addProperty("mac",mc);
			SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
			snv.dotNet=true;
			snv.setOutputSoapObject(soap);
			HttpTransportSE htp=new HttpTransportSE( Login.url);
			htp.call(soapaction, snv);
			String result=snv.getResponse().toString();
			Toast.makeText(getApplicationContext(), "reus="+result,Toast.LENGTH_LONG).show();
			if(result .equalsIgnoreCase("ok"))
			{
				Toast .makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
				Intent i=new Intent(getApplicationContext(),Block_view.class);
				startActivity(i);
				
			}
		}
			catch (Exception e)
			{
				Toast .makeText(getApplicationContext(), "errror"+e.getMessage(),Toast.LENGTH_LONG).show();
			}
			
		
		
	}

}
