package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class Command extends Activity implements OnClickListener {
	String namespace="http://tempuri.org/";
	String method="insert_comm";
	String soapaction="http://tempuri.org/insert_comm";

    Button b1,b2;
    Spinner sp1;
    String[]ss={"shutdown","restart","logoff"};
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_command);
	    b1=(Button)findViewById(R.id.b9);
	    b2=(Button)findViewById(R.id.button2);
	    sp1=(Spinner)findViewById(R.id.spinner1);
	    b1.setOnClickListener(this);
	    try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
	ArrayAdapter<String>ad=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,ss);
	  sp1.setAdapter(ad);  
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.command, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		if(arg0==b1)
		{
			SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String mm=sh.getString("mac", "");
			String cm=sp1.getSelectedItem().toString();
			try
			{
				SoapObject soap=new SoapObject(namespace, method);
				soap.addProperty("mac", mm);
				soap.addProperty("cmds",cm);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
					Intent i=new Intent(getApplicationContext(), View_system.class);
					startActivity(i);
				
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		
			
		}
		
	}

}
