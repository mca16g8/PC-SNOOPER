package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Keylog extends Activity implements OnItemClickListener {
	String namespace="http://tempuri.org/";
	String method="view_keylog";
	String soapaction="http://tempuri.org/view_keylog";
	int pos=0;
	String[] macc,k_id,k_name,date,time;

	ListView lv;
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_keylog);
		lv=(ListView)findViewById(R.id.listView1);
		lv.setOnItemClickListener(this);
		
		try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
	 try
		{
		 SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 String mc=sh.getString("mac", "");
		// Toast.makeText(getApplicationContext(), "mac="+mc, Toast.LENGTH_LONG).show();
			SoapObject soap=new SoapObject(namespace, method);
		soap.addProperty("mac", mc);
			SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
			snv.dotNet=true;
			snv.setOutputSoapObject(soap);
			HttpTransportSE h=new HttpTransportSE(Login.url);
			h.call(soapaction, snv);
			String res=snv.getResponse().toString();
			//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
			if(res.equalsIgnoreCase("no"))
			{
				Toast.makeText(getApplicationContext(), "no value", Toast.LENGTH_LONG).show();
			}
			else
			{
				String[]ar=res.split("@");
				String[]k_id=new String[ar.length];
				String[] macc=new String[ar.length];
				String[]k_name=new String[ar.length];
				String[]date=new String[ar.length];
				String[]time=new String[ar.length];
				
				
				for(int i=0;i<ar.length;i++)
				{
					String[]ar1=ar[i].split("#");
					k_id[i]=ar1[0];
					macc[i]=ar1[1];
					k_name[i]=ar1[2];
					date[i]=ar1[3];
				    time[i]=ar1[4];
				    
				   
					
					
					
				}
				//ArrayAdapter<String>ad=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,date);
				//lv.setAdapter(ad);
				lv.setAdapter(new Custom(getApplicationContext(),k_id,k_name));
			}
			
		}
		catch(Exception ex)
		{
			Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		}
	
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.keylog, menu);
		return true;
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stubpos=arg2;
		pos=arg2;
		final String a=k_name[pos];
		final String b=date[pos];
		final String c=time[pos];
		
		    SharedPreferences sh=PreferenceManager .getDefaultSharedPreferences(getApplicationContext());
		Editor ed=sh.edit();
		ed.putString("k_name",a);
		ed.putString("kdate", b);
		ed.putString("ktime", c);
		ed.commit();

			
			
	    	AlertDialog.Builder builder = new AlertDialog.Builder(Keylog.this);
	    	
			    builder.setTitle("options");
			    builder.setItems(new CharSequence[]
			            {"Key Details","Cancel"},
			            new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int which) {
			                    // The 'which' argument contains the index position
			                    // of the selected item
			                    switch (which) {
			                        case 0:
			                            Toast.makeText(getApplicationContext(), "Key Details", Toast.LENGTH_LONG).show();
			                            Intent i=new Intent(getApplicationContext(),Keydetails.class);
			                            startActivity(i);
			                           
			                            break;
			                       
			                        
			                        case 1:
			                        	
			                        	Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
			                    
			                        	 break;
			                   			                        	
			                      
			                    }
			                }
			    });
			    builder.create().show();

		
	}

}
