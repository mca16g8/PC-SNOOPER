package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Block_view extends Activity implements OnItemClickListener, OnClickListener {
	ListView lv;
Button b1;
	String namespace="http://tempuri.org/";
	String method="view_block";
	String soapaction="http://tempuri.org/view_block";
	//String url="http://169.254.247.135/PC%20SNOOPER/WebService.asmx";
	String method1="unblock";
	String soapaction1="http://tempuri.org/unblock";
	
	String []ar,si,ma;
	int pos;


	@Override
	public void onBackPressed() {
		Intent i=new Intent(getApplicationContext(),View_system.class);
		startActivity(i);
		
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_block_view);
		lv=(ListView)findViewById(R.id.listView1);
		lv.setOnItemClickListener(this);
		b1=(Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		try
		{
			if(android.os.Build.VERSION.SDK_INT>9)
			{
				ThreadPolicy th=new StrictMode .ThreadPolicy.Builder().permitAll().build();
				android.os.StrictMode .setThreadPolicy(th);
			}
			}
		catch(Exception ex)
		{
			Toast.makeText(getApplicationContext(), "error"+ex.getMessage(),Toast.LENGTH_LONG).show();
			
			
			
		}
		SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String s=sh.getString("mac", "");
		try
		{
			SoapObject soap=new SoapObject(namespace,method);
			soap.addProperty("mac", s);
			
			
			SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
			snv.dotNet=true;
			snv.setOutputSoapObject(soap);
			HttpTransportSE htp=new HttpTransportSE(Login.url);
			htp.call(soapaction, snv);
			String result=snv.getResponse().toString();
			//Toast.makeText(getApplicationContext(), "reus="+result,Toast.LENGTH_LONG).show();
            if(result.equalsIgnoreCase("no"))
				
			{
				Toast.makeText(getApplicationContext(), "no result",Toast.LENGTH_LONG).show();
			}
			else
			{
				String [] ar=result.split("\\@");
				si=new String[ar.length];
				ma=new String[ar.length];
				
				for(int i=0;i<ar.length;i++)
				{
					String [] ar1=ar[i].split("\\#");
					si[i]=ar1[0];
					ma[i]=ar1[1];
					
				}
				lv.setAdapter(new Custom(getApplicationContext(), si, ma));
	}

		
	
		}
		catch (Exception e)
		
		{
			Toast .makeText(getApplicationContext(), "errror"+e.getMessage(),Toast.LENGTH_LONG).show();
		}

		
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(Block_view.this);
    	
	    builder.setTitle("options");
	    builder.setItems(new CharSequence[]
	            {"Unblock","Cancel"},
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) {
	                    // The 'which' argument contains the index position
	                    // of the selected item
	                    switch (which) {
                        case 0:

                    		
                    		
                    		try
                    		{	SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    		    String s=sh.getString("mac", "");
                    			SoapObject soap=new SoapObject(namespace,method1);
                    			soap.addProperty("mac",s);
                    			soap.addProperty("blck", si[arg2]);
                    			
                    			SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    			snv.dotNet=true;
                    			snv.setOutputSoapObject(soap);
                    			HttpTransportSE htp=new HttpTransportSE(Login.url);
                    			htp.call(soapaction1, snv);
                    			String result=snv.getResponse().toString();
                    		   if(result.equalsIgnoreCase("ok"))
                    				
                    			{
                    				Toast.makeText(getApplicationContext(), "Deleted",Toast.LENGTH_LONG).show();
                    				Intent intt= new Intent(getApplicationContext(), Block_view.class);
                    				startActivity(intt);
                    			}
                    			else
                    			{
                    				Toast.makeText(getApplicationContext(), "Failed to delete",Toast.LENGTH_LONG).show();
                    			}
                    			}
                    		catch(Exception ex)
                    		{
                    			
                    		} 	
	                		
	                			
	                		
	                	
                   	break;
	                       
	                        
	                        case 1:
	                        	
	                        	Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
	                    
	                        	 break;
	                   			                        	
	                      
	                    }
	                }
	    });
	    builder.create().show();
		
		
		
		
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(b1==arg0)
		{
			Intent i=new Intent(getApplicationContext(),Block.class);
			startActivity(i);
		}
	}

}
