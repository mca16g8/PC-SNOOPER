package com.example.pcsnooper_mptc;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.widget.EditText;

public class Keydetails extends Activity {
EditText ed1,ed2,ed3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_keydetails);
		ed1=(EditText)findViewById(R.id.editText1);
		ed2=(EditText)findViewById(R.id.editText2);
		ed3=(EditText)findViewById(R.id.editText3);
		SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String s=sh.getString("k_name", "");
		String k=sh.getString("date", "");
		String l=sh.getString("time", "");
		ed1.setText(s);
		ed2.setText(k);
		ed3.setText(l);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.keydetails, menu);
		return true;
	}

}
