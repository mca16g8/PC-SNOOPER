package com.example.pcsnooper_mptc;

import android.R.color;
import android.R.layout;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Custom extends BaseAdapter{

	private Context Context;
	String[] c;
	String[] d;

	public Custom(Context applicationContext, String[] date, String[] not) {

		this.Context=applicationContext;
		this.c=date;
		this.d=not;
		
	}

	@Override
	public int getCount() {
		
		return d.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertview, ViewGroup parent) {

		
		LayoutInflater inflator=(LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View gridView;
		if(convertview==null)
		{
			gridView=new View(Context);
			//gridView=inflator.inflate(R.layout.customview, null);
			gridView=inflator.inflate(R.layout.activity_cu2, null);
			
		}
		else
		{
			gridView=(View)convertview;
			
		}
		
		TextView tv1=(TextView)gridView.findViewById(R.id.textView1);
		TextView tv2=(TextView)gridView.findViewById(R.id.textView2);
		
		tv1.setTextColor(Color.BLACK);
		tv2.setTextColor(Color.BLACK);
		
		tv1.setText(c[position]);
		tv2.setText(d[position]);
		
		
		return gridView;
	}

}
				