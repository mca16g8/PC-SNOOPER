package com.example.pcsnooper_mptc;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class FilesFetch extends Activity implements OnItemClickListener, OnItemLongClickListener, OnClickListener {
	ListView list;
	Button btn;
	EditText txt;
	final String ip=View_system.ipadr;
	final int port=5556;
	String[] folder;
	String cmd1;
	int k;
	
	

	final Context con = this;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_files_fetch);
		list=(ListView)findViewById(R.id.listView1);
		btn=(Button)findViewById(R.id.button1);
		txt=(EditText)findViewById(R.id.editText1);
		
		
		cmd1=getIntent().getStringExtra("cmd");
		
		mytask my=new mytask();
		my.execute("folder");
		
		list.setOnItemClickListener(this);
		list.setOnItemLongClickListener(this);//for long press event
		btn.setOnClickListener(this);
	}
	
	public class mytask extends AsyncTask<String, Void, String> {
			 
	    protected String doInBackground(String... str)
	    {
	    	String fldr="";
	   	 try{
	   	 		Socket client = new Socket(ip,port);  //connect to server
				PrintWriter printwriter = new PrintWriter(client.getOutputStream(),true);
				 printwriter.write(str[0]+"-"+cmd1);  //write the message to output stream
				 Log.d("hh", "hhhhhhhhhhhhh");
				 printwriter.flush();
				 
				
				 
				if(str[0]=="folder")
				{
					 BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
			     fldr =inFromClient.readLine();
			     folder=fldr.split("-");
			     

				}
				else if(str[0]=="search")
				{
					 BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
			     fldr =inFromClient.readLine();
			     folder=fldr.split("#");
			     

				}
				else if(str[0]=="save")
			     {
					 BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
					 
				 byte[]  buff=new byte[8888888];
						int br=0;
				String tl="";
				String ima="";
						 int cnt=0;
						// cnt=client.getInputStream().read(buff);
						 while((tl=inFromClient.readLine())!=null)
						 {
							 ima+=tl;
						 }
						client.close();
						 buff=Base64.decode(ima, Base64.DEFAULT);
						 
						 File f = new File(Environment.getExternalStorageDirectory() + "/RPC");
						 if(!f.mkdirs()) {
							 f.mkdir();
						 }
			   	
			    	 String filename = "/RPC/"+cmd1.substring(cmd1.lastIndexOf("\\")+1); 
			    	 
			    	 
			    	 java.io.File file = new java.io.File(Environment.getExternalStorageDirectory().getPath()+filename);
			    	 FileOutputStream fos = new FileOutputStream(file);
			    	 //byte[] bytearray = fldr.getBytes();
			    	 fos.write(buff);
			    	 fos.flush();
			    	 fos.close();
			    	 fldr="saving";
					  
			     }

					
				       
				 client.close();
	   	 }
	   	 catch(Exception ex)
	   	 {
	   		 ex.printStackTrace();
	   	 }
	   	 

	         return fldr;
	    }

	    protected void onProgressUpdate(Integer... progress) {
	       // setProgressPercent(progress[0]);
	    }

	    @Override
	    protected void onPostExecute(String result) {
	    	// TODO Auto-generated method stub
	    	super.onPostExecute(result);
	    	ArrayAdapter<String> adpt = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, result.split("-"));
			list.setAdapter(adpt);
	    }
	    
	    
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		if(k!=1)
		{
		Intent i=new Intent(getApplicationContext(),FilesFetch.class);
		i.putExtra("cmd", folder[arg2]);
		startActivity(i);
		}
		
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int ar2,
			long arg3) 
	{
		k=1;
		
		final String items[] = { "open","save", "delete"};
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);

		// set title
		alertDialogBuilder.setTitle("Choose Category").setItems(items, new DialogInterface.OnClickListener() 
		{
		               public void onClick(DialogInterface dialog, int which) 
		               {
		               // The 'which' argument contains the index position
		               // of the selected item
		            	   
		            	  String cat=items[which];
		            	  Toast.makeText(getApplicationContext(), cat, Toast.LENGTH_LONG).show();
		            	  if(cat=="save")
		            	  {
		            		  cmd1=folder[ar2];
		            		  mytask my=new mytask();
		            			my.execute("save");
	            		  
		            	  }
		            	  else if(cat=="delete")
		            	  {
		            		  cmd1=folder[ar2];
		            		  mytask my=new mytask();
		            			my.execute("delete");
		            			
		            	  }
		            	  else if(cat=="open")
		            	  {
		            		  cmd1=folder[ar2];
		            		  mytask my=new mytask();
		            			my.execute("open");
		            	  }
		               }
		               });

// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		
		return false;
	}
	String filen="";
	
	@Override
	public void onClick(View arg0) {
		Toast.makeText(getApplicationContext(), "oookkk", Toast.LENGTH_LONG).show();
		
	 filen=txt.getText().toString();
		 String cmmd=cmd1;
		 cmd1=filen+"@"+cmmd;
		  mytask my=new mytask();
			my.execute("search");
}
}