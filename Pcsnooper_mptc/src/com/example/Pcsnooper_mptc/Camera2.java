package com.example.pcsnooper_mptc;

import java.net.URI;
import java.net.URL;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Camera extends Activity implements OnClickListener {
	Button b1,b2;
	ImageView img;
	String namespace="http://tempuri.org/";
	String method="insert_camera";
	String soapaction="http://tempuri.org/insert_camera";

	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);
		b1=(Button)findViewById(R.id.b1);
	    b2=(Button)findViewById(R.id.b2);
	    img=(ImageView)findViewById(R.id.img1);
	    b1.setOnClickListener(this);
	    b2.setOnClickListener(this);
	    try
	    {
	    if(android.os.Build.VERSION.SDK_INT>9)
	    {
	    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	android.os.StrictMode.setThreadPolicy(th);
	    }}
	    catch(Exception ex)
	    {
	    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) 
	{
		// TODO Auto-generated method stub
		if(arg0==b1)
		{
			
			try
			{
				SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			     String mc=sh.getString("mac", "");
				 //Toast.makeText(getApplicationContext(), "mac="+mc, Toast.LENGTH_LONG).show();
				SoapObject soap=new SoapObject(namespace, method);
				soap.addProperty("mac", mc);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
				String res=snv.getResponse().toString();
				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
			
					
				
				
				
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		
	}
		if(arg0==b2)
		{
			SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		     String mc=sh.getString("mac", "");
		     String ip=sh.getString("ip", "");
		     //Toast.makeText(getApplicationContext(), "mac="+mc, Toast.LENGTH_LONG).show();
		    // Toast.makeText(getApplicationContext(), "ip="+ip, Toast.LENGTH_LONG).show();
			 String ss="http://"+ip+"/pcsnooper%20madin1/cam"+mc+".jpg";
			//Toast.makeText(getApplicationContext(), "ss="+ss, Toast.LENGTH_LONG).show();
			try
			{
				URL u=new URL(ss);
				Bitmap bmp=BitmapFactory.decodeStream(u.openStream());
				img.setImageBitmap(bmp);
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
			
		}

	}
}