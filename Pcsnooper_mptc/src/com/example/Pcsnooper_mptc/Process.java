package com.example.pcsnooper_mptc;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Process extends Activity implements OnItemClickListener {
	String namespace="http://tempuri.org/";
	String method="view_process";
	String soapaction="http://tempuri.org/view_process";
	String[]name,date,time;
	
	ListView lv;
	int pos;
	@TargetApi(Build.VERSION_CODES.GINGERBREAD) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_process);
		lv=(ListView)findViewById(R.id.listView1);
		lv.setOnItemClickListener(this);
		
		 try
		    {
		    if(android.os.Build.VERSION.SDK_INT>9)
		    {
		    	ThreadPolicy th=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    	android.os.StrictMode.setThreadPolicy(th);
		    }}
		    catch(Exception ex)
		    {
		    	Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		    }
		 try
			{
			 SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			 String mc=sh.getString("mac", "");
			 
			 //Toast.makeText(getApplicationContext(), "mac", Toast.LENGTH_LONG).show();
				SoapObject soap=new SoapObject(namespace, method);
			soap.addProperty("mac", mc);
				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
				snv.dotNet=true;
				snv.setOutputSoapObject(soap);
				HttpTransportSE h=new HttpTransportSE(Login.url);
				h.call(soapaction, snv);
			String res=snv.getResponse().toString();
				//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
				if(res.equalsIgnoreCase("no"))
				{
					Toast.makeText(getApplicationContext(), "no value", Toast.LENGTH_LONG).show();
				}
				else
				{
				String[]ar=res.split("\\@");
					date=new String[ar.length];
					time=new String[ar.length];
					name=new String[ar.length];
					for(int i=0;i<ar.length;i++)
					{
						String[]ar1=ar[i].split("\\#");
						name[i]=ar1[1];
						date[i]=ar1[3];
					    time[i]=ar1[4];
					    
					    
						
						
					}
					//ArrayAdapter<String> ad=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,date);
				//.setAdapter(ad);
				lv.setAdapter(new Customthree(getApplicationContext(),name,date,time));
				}
				
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		
			
		}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.process, menu);
		return true;
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	
	pos=arg2;


	if(arg0==lv)
	{
		final String name1=name[pos];
		SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		final String mc1=sh.getString("mac", "");
		
		
		
    	AlertDialog.Builder builder = new AlertDialog.Builder(Process.this);
    	
		    builder.setTitle("options");
		    builder.setItems(new CharSequence[]
		            {"Kill process","Cancel"},
		            new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int which) {
		                    // The 'which' argument contains the index position
		                    // of the selected item
		                    switch (which) {
	                        case 0:
		                        	String method="insert_kill";
		                			String soapaction="http://tempuri.org/insert_kill";
		                			
		                			try
		                			{
		                				SoapObject soap=new SoapObject(namespace, method);
		                				soap.addProperty("name", name1);
		                				soap.addProperty("mac", mc1);
		                				SoapSerializationEnvelope snv=new SoapSerializationEnvelope(SoapEnvelope.VER11);
		                				snv.dotNet=true;
		                				snv.setOutputSoapObject(soap);
		                				HttpTransportSE h=new HttpTransportSE(Login.url);
		                				h.call(soapaction, snv);
		                				String res=snv.getResponse().toString();
		                				//Toast.makeText(getApplicationContext(), "res="+res, Toast.LENGTH_LONG).show();
		                				if(res.equalsIgnoreCase("ok"))
		                				{
		                					Toast.makeText(getApplicationContext(), "success",Toast.LENGTH_LONG).show();
		                					Intent i=new Intent(getApplicationContext(), Process.class);
		                					startActivity(i);
		                				
		                				}
		                				
		                			}
		                			catch(Exception ex)
		                			{
		                				Toast.makeText(getApplicationContext(), "err"+ex.getMessage(), Toast.LENGTH_LONG).show();
		                			}
		                		
		                			
		                		
		                	
                       	break;
		                       
		                        
		                        case 1:
		                        	
		                        	Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
		                    
		                        	 break;
		                   			                        	
		                      
		                    }
		                }
		    });
		    builder.create().show();
}
	}}
	
